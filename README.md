# GatherlyMobile

Android based application for our social media website [Gatherly](http://qav2.cs.odu.edu/fordFanatics/index.php). Any website is incomplete with a mobile presence. GatherlyMobile lets a user interact with their friends, share pictures, files, it even lets you create group with people you share common interest with. GatherlyMobile lets you chat with your friends live.

**Features:**
1. List of users on Gatherly, you can see who is active and available to chat.
2. List of groups, both public and private. Created by the user and their friends.
3. Global chat room.

Application was built on Android Studio 3.2.

**Application version:** 
1. Build tools version 26.0.2.
2. Minimum sdk version 15.

**Dependencies:** 
1. App compat.
2. Firebase client.
3. Firebase messaging.

**Installation:**<br/>
In order to install GatherlyMobile application on the android phone follow the steps mentioned below

1. Download the app-debug.apk on your local computer.
2. Open Android Studio<br/>
  a. Click on profile or debug apk.<br/>
  b. Select the app-debug.apk from your local computer.<br/>
  c. If prompted, create a new folder and press run.<br/>
3. You can use an emulator to test the application but to explore all of the features available on the application connect your      phone to the Android SDK and press, "Run".
4. The application will now be launched on your phone, go ahead and explore.


![ActiveUsers](./AppStatistics/Activeusers.png "Active user statistics after deployment.")
![PageUsage](./AppStatistics/Pageusage.png)
![StabilityofApplication](./AppStatistics/Stability.png)
![UserActivity](./AppStatistics/UserActivity.png)
![UserAdoption](./AppStatistics/userAdoption.png)

